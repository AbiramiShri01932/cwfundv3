var wh, ww, chart1, chart3, chart4, interactiveChart, controller;
$(document).ready(function () {
	wh = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;	
	$(".theater .col,.theater h1,.debt .timeline,.overview .col,.performance .col,.performance2 .content,.performance2 .t-contain,.performance2 .group,.performance2 .content:nth-child(2) h3,.portfolio .col,.portfolio .content,.facts .content,.literature .content,.invest .col").inView({keep:true,offset:-wh/2});
	
  $("#btn-menu").click(function () {
    $(this).toggleClass("on");
    $("body").toggleClass("nav-on");
    if ($("body").hasClass("nav-on")) $("body").removeClass("scroll");
    else if ($(window).scrollTop() > 0) $("body").addClass("scroll");
  });
  $("nav.n2 a,.nav a,.theater a").click(function() {
  	scrollTo($("#container section."+$(this).data("id")).offset().top+1);
  	if($("body").hasClass("nav-on")) $("#btn-menu").click();
  });
  $(window).scroll(function() {
  	fireTimer("scrollf",scrollFunction,100);
  }).scroll();
	
  $(".btn-contact,.link-contact").click(function() {
  	$("#contact,#shade").addClass("on");
  	$("#login").removeClass("on");
  });
  $(".btn-close").click(function() {
  	$("#contact,#shade,#login").removeClass("on");
  });
  $("#shade").click(function() {
  	$(".btn-close:eq(0)").click();
  });
  $("a.locked").click(function(e) {
  	e.preventDefault();
  	$("#contact,#shade").addClass("on");
  	$("#login").removeClass("on");
  });
  $("sup","#container").click(function() {
  	var v=$(this).text();
  	$("sup",".footnotes").each(function() {
  		if($(this).text() == v)
  		{
  			scrollTo($(this).offset().top);
  			return false;	
  		}
  	});
  });
  
});
function scrollMagicInit()
{
	if(!controller) controller = new ScrollMagic.Controller();
	var chartOptions={};
	  
	  function step1()
	  {
	  	if(chartOptions["plot1"])
	  	{
	  		chart1.xAxis[0].removePlotBand('plot-band-1');
		  	chartOptions["plot1"]=0; 
	  	}
	  }
	  function step2()
	  {
	  	if(!chartOptions["plot1"])
	  	{
	  		chartOptions["plot1"]=1;
	  		setTimeout(function() {
	  		chartOptions["plot1"] = chart1.xAxis[0].addPlotBand({
	                                    from: 8,
	                                    to: 10,
	                                    color: '#073358',
	                                    id: 'plot-band-1'
	                                });
	       setTimeout(function() { $(chartOptions["plot1"].svgElem.element).css("opacity",1); },20);
	      },500);
	  	}
	    $(".performance #table1").removeClass("onA");
	  }
	  function step3()
	  {
	  	$(".performance #table1").addClass("onA").removeClass("onB");
	  }
	  function step4()
	  {
	  	$(".performance #table1").addClass("onB");
		  		
  		if(chartOptions["plot2"])
  		{
  			chart1.xAxis[0].removePlotBand('plot-band-2');
  			chartOptions["plot2"]=0;
  		}	
	  }
	  function step5()
	  {
	  	if(!chartOptions["plot2"])
	  	{
	  		chartOptions["plot2"]=1;
	  		setTimeout(function() {
	  			chartOptions["plot2"] = chart1.xAxis[0].addPlotBand({
	                                    from: 13,
	                                    to: 22,
	                                    color: '#073358',
	                                    id: 'plot-band-2'
	                                });
	        setTimeout(function() { $(chartOptions["plot2"].svgElem.element).css("opacity",1); },20);
	      }, 500);  
	    }    
      $(".performance #table2").removeClass("onA");
	  }
	  function step6()
	  {
	  	$(".performance #table2").addClass("onA").removeClass("onB");
	  }
	  function step7()
	  {
	  	$(".performance #table2").addClass("onB");
	  }
		interactiveChart = new ScrollMagic.Scene({
		  triggerElement: "#interactiveChart",
		   triggerHook: "onLeave",
		  duration: 2000,
		  offset: -130
		}).setPin("#interactiveChart")
		  .on("progress", function (e) {
		  	var p=e.progress.toFixed(3)*1000;
		  	if(p<142 && chartOptions["plot1"])
		  	{
		  		step1();
		  	}
		  	else if(p>=142 && p<284)
		  	{
		  		step2();
		  	}
		  	else if(p>=284 && p<426)
		  	{
		  		if(!chartOptions["plot1"]) step2();
		  		step3();
		  	}
		  	else if(p>=426 && p<568)
		  	{
		  		if(!chartOptions["plot1"]) step2();
		  		if(!$(".performance #table1.onA").length) step3();
		  		step4();
		  	}
		  	else if(p>=568 && p<710)
		  	{
		  		if(!chartOptions["plot1"]) step2();
		  		if(!$(".performance #table1.onA").length) step3();
		  		if(!$(".performance #table1.onB").length) step4();
		  		step5();
		  	}
		  	else if(p>=710 && p<852)
		  	{
		  		if(!chartOptions["plot1"]) step2();
		  		if(!$(".performance #table1.onA").length) step3();
		  		if(!$(".performance #table1.onB").length) step4();
		  		if(!chartOptions["plot2"]) step5();
		  		step6();
		  	}
		  	else if(p>=852)
		  	{
		  		if(!chartOptions["plot1"]) step2();
		  		if(!$(".performance #table1.onA").length) step3();
		  		if(!$(".performance #table1.onB").length) step4();
		  		if(!chartOptions["plot2"]) step5();
		  		if(!$(".performance #table2.onA").length) step6();
		  		step7();
		  	}
			}).addTo(controller);
}
function scrollFunction() {
  var t = $(window).scrollTop();
  if (t > 0) $("body").addClass("scroll");
  else $("body").removeClass("scroll overtop intheater");
  	
  if(t>wh) $("body").addClass("overtop").removeClass("intheater");
  if(t<wh && $("body").hasClass("overtop")) $("body").addClass("intheater"); 
  var top_of_element = $(".overview").offset().top;
  var bottom_of_element = $(".overview").offset().top + $(".overview").outerHeight();
  var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
  var top_of_screen = $(window).scrollTop();

  if (top_of_screen >577){
	  $("nav.n1").hide()
$("nav.n2").show();

  } else {
	  $("nav.n2").hide()
$("nav.n1").show()
$('#main-nav').css('margin-top',"0px");


  }
  $("#container section").each(function() {
  	if(t>=$(this).offset().top-$("header").outerHeight() && t<=$(this).offset().top+$(this).outerHeight()-$("header").outerHeight())
  	{
  		$("nav.n2 a").parent().removeClass("on");
  		var c=$(this).attr("class");
  		$("nav.n2 a[data-id="+c+"]").parent().addClass("on");
  	}
	//   if ($('.nav').visible(true)) {
	// 	  console.log("true");
	// 	$("body").removeClass("intheater"); 
	// } else {
	// 	console.log("false");

	// 	$("body").addClass("intheater"); 

	// }
  });
  //loading charts when in view
  if($("#chart1").length && t>=$("#chart1").offset().top-wh/2 && !$("#chart1").hasClass("loaded"))
  {
		$("#chart1").addClass("loaded");
		
		chart1 = Highcharts.chart('chart1', {
		
			chart: {backgroundColor: "transparent"}, //#011E40
			data: {
        csvURL: '/d/chart1.csv',
        firstRowAsNames: false,
        itemDelimiter:",",
        lineDelimiter: "\n"
    	},

			title: {
			        text: ''
			    },
			exporting: { enabled: false },
	    yAxis: {
	        title:'',
					labels: {
						formatter: function() {
							return this.value + '%';
						},
						style:{fontSize:'17px','color':'#ffffff','fontFamily':'verlag-light'}
					},
					gridLineColor:'#565B61',
					tickAmount: 5
					
	    },
	 
	    xAxis: {
	       labels: {
	        useHTML : true,
	        title: {
	            enabled: false
	        },
	        style:{fontSize:'17px','color':'#ffffff','fontFamily':'verlag-light'}        
	    	},
	    	lineColor:'#565B61',
	    	type:'category'
	    	
	    },
	
	    legend: {
	        layout: 'horizontal',
	        align: 'right',
	        verticalAlign: 'top',
	        enabled:true,
	        useHTML:true,
	        itemStyle:{fontSize:'19px','color':'#ffffff','fontFamily':'verlag-xlight'} 
	    },
	
	    plotOptions: {
	         series: {
            marker: {
                enabled: false
            }
        }
	    },
			
	    series: [{
	        name: 'CCFLX',
	        color:'#32B5EB',
	        dataLabels:{enabled:false,format:'{y}%',verticalAlign:'top',color:'#ffffff',style:{fontFamily:'verlag-light',fontSize:'19px'}},
	        label:{enabled:false}
	    }, {
	        name: 'Investment Grade Bonds<sup>8</sup>',
	        color:'#ffffff',
	        dataLabels:{enabled:false,format:'{y}%',verticalAlign:'top',color:'#ffffff',style:{fontFamily:'verlag-light',fontSize:'19px'}},
	        label:{enabled:false}
	    }]
    });	
		
  }
  if($("#chart3").length && t>=$("#chart3").offset().top-wh/2 && !$("#chart3").hasClass("loaded"))
  {
		$("#chart3").addClass("loaded");
		
		chart3 = Highcharts.chart('chart3', {
		
			chart: {backgroundColor: "transparent"}, //#011E40
			data: {
        csvURL: '/d/chart3.csv',
        firstRowAsNames: false,
        itemDelimiter:",",
        lineDelimiter: "\n"
    	},

			title: {
			        text: ''
			    },
			exporting: { enabled: false },
	    yAxis: {
	        title:'',
					labels: {
						formatter: function() {
							return this.value + '%';
						},
						style:{fontSize:'17px','color':'#ffffff','fontFamily':'verlag-light'}
					},
					gridLineColor:'#565B61',
					tickAmount: 5
					
	    },
	 
	    xAxis: {
	       labels: {
	        useHTML : true,
	        title: {
	            enabled: false
	        },
	        style:{fontSize:'17px','color':'#ffffff','fontFamily':'verlag-light'}        
	    	},
	    	lineColor:'#565B61',
	    	type:'category'
	    	
	    },
	
	    legend: {
	        layout: 'horizontal',
	        align: 'right',
	        verticalAlign: 'bottom',
	        enabled:true,
	        useHTML:true,
	        itemStyle:{fontSize:'19px','color':'#ffffff','fontFamily':'verlag-xlight'} 
	    },
	
	    plotOptions: {
	         series: {
            marker: {
                enabled: false
            }
        }
	    },
			
	    series: [{
	        name: 'CCFLX',
	        color:'#32B5EB',
	        dataLabels:{enabled:false,format:'{y}%',verticalAlign:'top',color:'#ffffff',style:{fontFamily:'verlag-light',fontSize:'19px'}},
	        label:{enabled:false}
	    }, {
	        name: 'Investment Grade Bonds<sup>8</sup>',
	        color:'#ffffff',
	        dataLabels:{enabled:false,format:'{y}%',verticalAlign:'top',color:'#ffffff',style:{fontFamily:'verlag-light',fontSize:'19px'}},
	        label:{enabled:false}
	    }]
    });	
		
  }
  if($("#chart4").length && t>=$("#chart4").offset().top-wh/2 && !$("#chart4").hasClass("loaded"))
  {
		$("#chart4").addClass("loaded");
		
		chart4 = Highcharts.chart('chart4', {
		
			chart: {backgroundColor: "transparent"}, //#011E40
			data: {
        csvURL: '/d/chart4.csv',
        firstRowAsNames: false,
        itemDelimiter:",",
        lineDelimiter: "\n"
    	},

			title: {
			        text: ''
			    },
			exporting: { enabled: false },
	    yAxis: {
	        title:'',
					labels: {
						formatter: function() {
							return this.value + '%';
						},
						style:{fontSize:'17px','color':'#ffffff','fontFamily':'verlag-light'}
					},
					gridLineColor:'#565B61',
					tickAmount: 5
					
	    },
	 
	    xAxis: {
	       labels: {
	        useHTML : true,
	        title: {
	            enabled: false
	        },
	        style:{fontSize:'17px','color':'#ffffff','fontFamily':'verlag-light'}        
	    	},
	    	lineColor:'#565B61',
	    	type:'category'
	    	
	    },
	
	    legend: {
	        layout: 'horizontal',
	        align: 'right',
	        verticalAlign: 'bottom',
	        enabled:true,
	        useHTML:true,
	        itemStyle:{fontSize:'19px','color':'#ffffff','fontFamily':'verlag-xlight'} 
	    },
	
	    plotOptions: {
	         series: {
            marker: {
                enabled: false
            }
        }
	    },
			
	    series: [{
	        name: 'CCFLX',
	        color:'#32B5EB',
	        dataLabels:{enabled:false,format:'{y}%',verticalAlign:'top',color:'#ffffff',style:{fontFamily:'verlag-light',fontSize:'19px'}},
	        label:{enabled:false}
	    }, {
	        name: 'Investment Grade Bonds<sup>8</sup>',
	        color:'#ffffff',
	        dataLabels:{enabled:false,format:'{y}%',verticalAlign:'top',color:'#ffffff',style:{fontFamily:'verlag-light',fontSize:'19px'}},
	        label:{enabled:false}
	    }]
    });	
		
  }
  if(t>=$("#chart2").offset().top-wh/2 && !$("#chart2").hasClass("loaded"))
  {
			Highcharts.chart('chart2', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: ''
			    },
			    exporting: { enabled: false },
			    xAxis: {
			        categories: ['Annualized (Since Inception<sup>6</sup>)', 'YTD'],
			        labels: {
                useHTML : true,
                title: {
                    enabled: false
                },
                style:{fontSize:'22px','color':'#606771','fontFamily':'verlag-light'}        
            	},
            	lineColor:'#29A9E4',
	            plotLines: [{
	            color: '#D4DAE8',
	            width: 1,
	            value: 0.5
	        		}]
            	
			    },
			    yAxis: {
			    	title:'',
						labels: {
							formatter: function() {
								return this.value + '%';
							},
							style:{fontSize:'19px','fontFamily':'verlag-light'}
						},
						tickAmount: 4
					},
			    legend: {
                enabled:true,
                useHTML:true,
               	itemStyle:{letterSpacing:'0.2em',fontSize:'13px','color':'#606771','textTransform':'uppercase','fontFamily':'verlag-light'} 
          },      
			    series: [{
			        name: 'Investment Grade Bonds<sup>8</sup>',
			        data: [4.26, -2.67],
			        color:'#A6A6A6',
			        dataLabels:{useHTML:true,enabled:true,format:'{y}%',inside:true,verticalAlign:'top',color:'#1f334c',style:{fontFamily:'verlag-light',fontSize:'19px'}}
			        
			    }, {
			        name: 'Liquid Loans<sup>9</sup>',
			        data: [2.48, 0.52],
			        color:'#7F7F7F',
			        dataLabels:{useHTML:true,enabled:true,format:'{y}%',inside:true,verticalAlign:'top',color:'#1f334c',style:{fontFamily:'verlag-light',fontSize:'19px'} }
			    }, {
			        name: 'CCFLX',
			        data: [8.29, 3.88],
			        color:'#29AAE4',
			        dataLabels:{useHTML:true,enabled:true,format:'{y}%',inside:true,verticalAlign:'top',color:'#1f334c',style:{fontFamily:'verlag-light',fontSize:'19px'}}
			    }]
			});
		$("#chart2").addClass("loaded");
  }	
}
function scrollTo(n, a) {
  if (!a) a = 1500; /* this is the correct one */
  if (isNaN(n)) $("body,html").scrollTop(0);
  else
    $("body,html").animate(
      { scrollTop: n - $("header").outerHeight() },
      a
    );
}
$(window).resize(function() {
	wh = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	ww = document.documentElement.clientWidth || document.body.clientWidth;
	if(interactiveChart && ww<980)
	{
		interactiveChart.removePin();
		interactiveChart.destroy();
		//controller.destroy();
		  //controller = null;
		interactiveChart = 0;
	}
	else if(!interactiveChart && ww>979)
	{
		scrollMagicInit();
	}	
	
	if(ww>979)
	{
		chart1.update({
			legend: {verticalAlign: 'top'}	
		});
	}
	else
	{
		chart1.update({
			legend: {verticalAlign: 'bottom'}	
		});
		chart1.xAxis[0].removePlotBand('plot-band-1');
		chart1.xAxis[0].removePlotBand('plot-band-2');
	}	
}).resize();